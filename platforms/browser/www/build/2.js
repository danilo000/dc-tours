webpackJsonp([2],{

/***/ 686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationDetailsPageModule", function() { return LocationDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__location_details__ = __webpack_require__(691);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LocationDetailsPageModule = /** @class */ (function () {
    function LocationDetailsPageModule() {
    }
    LocationDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__location_details__["a" /* LocationDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__location_details__["a" /* LocationDetailsPage */]),
            ],
        })
    ], LocationDetailsPageModule);
    return LocationDetailsPageModule;
}());

//# sourceMappingURL=location-details.module.js.map

/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LocationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LocationDetailsPage = /** @class */ (function () {
    function LocationDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.location = this.navParams.get('location');
        console.log("here");
        console.log(this.location);
    }
    LocationDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LocationDetailsPage');
    };
    LocationDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-location-details',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/location-details/location-details.html"*/'<!--\n  Generated template for the LocationDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n    <ion-navbar color="primary">\n        <ion-title></ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content fullscreen>\n\n    <ion-segment [(ngModel)]="category">\n        <ion-segment-button value="gear">\n            Gear\n        </ion-segment-button>\n        <ion-segment-button value="clothing">\n            Clothing\n        </ion-segment-button>\n        <ion-segment-button value="nutrition">\n            Nutrition\n        </ion-segment-button>\n    </ion-segment>\n\n    <ion-card>\n\n        <ion-item>\n            <ion-avatar item-left>\n                <img src="https://avatars.io/facebook/joshua.morony" />\n            </ion-avatar>\n            <h2>Josh Morony</h2>\n            <p>November 5, 1955</p>\n        </ion-item>\n\n        <img src="http://placehold.it/500x200" />\n\n        <ion-card-content>\n            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.\n        </ion-card-content>\n\n\n    </ion-card>\n\n</ion-content>\n<!--\n<ion-content padding>\n    <ion-card-header>\n        Header\n    </ion-card-header>\n    <ion-card-content>\n        <div *ngFor="let details of location | async ">\n            <h2>{{ details | json }}</h2>\n        </div>\n        The British use the term "header", but the American term "head-shot" the English simply refuse to adopt.\n    </ion-card-content>\n\n    <ion-card>\n        <ion-card-content *ngFor="let details of location">\n\n            <h2>{{ details.name }}</h2>\n            <h3 margin-top>{{ details.description }} </h3>\n            <h5> {{ details | json }} </h5>\n\n\n        </ion-card-content>\n    </ion-card> -->'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/location-details/location-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], LocationDetailsPage);
    return LocationDetailsPage;
}());

//# sourceMappingURL=location-details.js.map

/***/ })

});
//# sourceMappingURL=2.js.map