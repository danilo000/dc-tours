webpackJsonp([5],{

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tab2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the Tab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Tab2Page = /** @class */ (function () {
    function Tab2Page(navCtrl, navParams, httpClient, geolocation, locationsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.geolocation = geolocation;
        this.locationsProvider = locationsProvider;
        this.descending = false;
        this.column = 'name';
        this.urlDistances = 'assets/distances.json';
        //urlDistances = 'http://192.168.0.153:7071/api/Distances?position=';
        //url = 'http://192.168.0.153:7071/api/Locations';
        this.url = 'assets/locations.json';
        this.items = [];
        for (var x = 0; x < 5; x++) {
            this.items.push(x);
        }
        this.loadLocations();
        //this.locations = this.httpClient.get(this.url);
        //this.distances = this.httpClient.get(this.urlDistances+"54.5999349"+","+"-5.99768");
        //this.distances = this.httpClient.get(this.urlDistances);
        //this.startTracking();
    }
    Tab2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Tab2Page');
    };
    Tab2Page.prototype.loadLocations = function () {
        var _this = this;
        this.locationsProvider.load()
            .then(function (data) {
            _this.locations = data;
            //console.log(data);
            //locations loaded
            _this.startTracking();
        });
    };
    Tab2Page.prototype.loadDistances = function (lat, lon) {
        var _this = this;
        this.locationsProvider.loadDistances(lat, lon)
            .then(function (data) {
            console.log(data);
            _this.distances = data;
            _this.updateView();
        });
    };
    Tab2Page.prototype.reorderItems = function (indexes) {
        var element = this.combinedArr[indexes.from];
        this.combinedArr.splice(indexes.from, 1);
        this.combinedArr.splice(indexes.to, 0, element);
    };
    Tab2Page.prototype.startTracking = function () {
        var _this = this;
        this.isTracking = true;
        this.trackedRoute = [];
        this.positionSubscription = this.geolocation.watchPosition()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["filter"])(function (p) { return p.coords !== undefined; }) //Filter Out Errors
        )
            .subscribe(function (data) {
            setTimeout(function () {
                console.log(data.coords.latitude + " - lat" + data.coords.longitude + " - long");
                //console.log(this.locations);
                //this.distances = this.httpClient.get(this.urlDistances+data.coords.latitude+","+data.coords.longitude);
                _this.loadDistances(data.coords.latitude, data.coords.longitude);
                //this.reorderItems(3);
                //Load it only once
            }, 0);
        });
    };
    Tab2Page.prototype.updateView = function () {
        //console.log("updating view");
        // if(!this.combinedArr)
        /*Observable.combineLatest(this.locations, this.distances).subscribe(
          ([ValOne, ValTwo]) => {
            console.log(ValOne);
            console.log(ValTwo);
            this.combinedArr = [];
            const length = Math.max(ValOne.length, ValTwo.length);
            for (let i = 0; i < length; i++) {
              //TODO: check if it is in an array of removed items -> then only push
              this.combinedArr.push([ValOne, ValTwo]);
            }
          }
        );*/
        var length = Math.max(this.locations.length, this.distances.length);
        if (!this.combinedArr) {
            this.combinedArr = [];
            for (var i = 0; i < length; i++) {
                this.combinedArr.push({
                    id: this.locations[i].id,
                    name: this.locations[i].name,
                    distance: this.distances[i].distance
                });
            }
        }
        else
            for (var i = 0; i < this.combinedArr.length; i++) {
                //update only distances
                var id = this.combinedArr[i].id;
                //console.log(this.isItemInArray(this.distances,id));
                this.combinedArr[i].distance = this.distances[this.isItemInArray(this.distances, id)].distance;
            }
        // console.log(this.combinedArr);
    };
    Tab2Page.prototype.isItemInArray = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            // depends on the format of the array
            if (array[i].id == item) {
                return i; // Found it
            }
        }
        return -1; // Not found
    };
    Tab2Page.prototype.start = function (page) {
        console.log("start");
        //filter
        var locs = [];
        for (var i = 0; i < this.combinedArr.length; i++) {
            locs[i] =
                {
                    id: this.combinedArr[i].id,
                    name: this.combinedArr[i].name,
                    distance: this.combinedArr[i].distance,
                    lat: this.locations[i].lat,
                    lon: this.locations[i].lon
                };
        }
        //console.log(locs.length)
        this.navCtrl.push('NavigatePage', { indexes: locs });
    };
    Tab2Page.prototype.sortList = function () {
        try {
            var count = this.combinedArr.length;
            var swapped = void 0;
            for (var i = 0; i < count; i++) {
                swapped = false;
                for (var j = 0; j < count - 1; j++) {
                    if (this.combinedArr[i].distance < this.combinedArr[j].distance) {
                        this.reorderItems({ from: i, to: j });
                        swapped = true;
                    }
                }
                if (!swapped) {
                    break;
                }
            }
        }
        catch (_a) { }
    };
    Tab2Page.prototype.deleteItem = function (item) {
        //TODO: write the value to an array for further check
        for (var i = 0; i < this.combinedArr.length; i++) {
            if (this.combinedArr[i] == item) {
                this.combinedArr.splice(i, 1);
            }
        }
    };
    Tab2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tab2',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tab2/tab2.html"*/'<!--\n  Generated template for the Tab2Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar>\n        <ion-title text-center>Set your tour</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n    <ion-title> Choose the order of your stops:</ion-title>\n\n    <ion-item margin-top>\n        <ion-label>\n            <ion-icon name="locate"></ion-icon>\n            Your current location\n            <h3 text-right> 0 m </h3>\n\n        </ion-label>\n\n    </ion-item>\n\n    <button float-right ion-button (click)="sortList()">Present an ideal path</button>\n\n    <ion-list reorder="true" (ionItemReorder)="reorderItems($event)">\n        <ion-item-sliding *ngFor="let location of combinedArr   ">\n\n            <ion-item>\n                <ion-label>\n                    <div text-wrap>\n                        <h2>{{ location.name }}</h2>\n                        <h3 text-right>{{ location.distance | number:\'1.0-0\' | distancePipe }} </h3>\n                    </div>\n                </ion-label>\n                <ion-reorder slot="end">\n                    <ion-icon name="pizza"></ion-icon>\n                </ion-reorder>\n            </ion-item>\n\n\n            <ion-item-options side="right">\n                <button ion-button color="danger" (click)="deleteItem(cols)">\n          <ion-icon name="trash"></ion-icon>\n          Remove\n        </button>\n            </ion-item-options>\n\n        </ion-item-sliding>\n    </ion-list>\n\n    <button ion-button block icon-end (click)="start(location)">\n    Start\n    <ion-icon name="navigate"></ion-icon>\n  </button>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tab2/tab2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__["a" /* LocationsProvider */]])
    ], Tab2Page);
    return Tab2Page;
}());

//# sourceMappingURL=tab2.js.map

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 166;

/***/ }),

/***/ 210:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/location-details/location-details.module": [
		686,
		2
	],
	"../pages/navigate/navigate.module": [
		690,
		4
	],
	"../pages/tab1/tab1.module": [
		687,
		1
	],
	"../pages/tabs/tabs.module": [
		688,
		3
	],
	"../pages/thankyou/thankyou.module": [
		689,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 210;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_phonegap_local_notification_ngx__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_slick_carousel__ = __webpack_require__(682);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_slick_carousel___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_slick_carousel__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_locations_locations__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__tab2_tab2__ = __webpack_require__(155);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, app, plt, geolocation, storage, push, httpClient, localNotification, locationsProvider) {
        this.navCtrl = navCtrl;
        this.app = app;
        this.plt = plt;
        this.geolocation = geolocation;
        this.storage = storage;
        this.push = push;
        this.httpClient = httpClient;
        this.localNotification = localNotification;
        this.locationsProvider = locationsProvider;
        this.currentMapTrack = null;
        this.isTracking = false;
        this.trackedRoute = [];
        this.previousTracks = [];
        //urlDistances = 'http://192.168.0.153:7071/api/Distances?position=';
        this.urlDistances = 'assets/distances.json';
        //url = 'http://192.168.0.153:7071/api/Locations';
        this.url = 'assets/locations.json';
        this.loadLocations();
        //this.locations = this.httpClient.get(this.url);
        //this.distances = this.httpClient.get(this.urlDistances+"54.5999349"+","+"-5.99768");
        //this.distances = this.httpClient.get(this.urlDistances);
        //this.startTracking();
        /*localNotifications.schedule({
          title: 'My first notification',
          text: 'Thats pretty easy...',
          foreground: true
      });*/
        //console.log(this.results);
        //this.updateDistances();
    }
    HomePage.prototype.loadLocations = function () {
        var _this = this;
        this.locationsProvider.load()
            .then(function (data) {
            _this.locations = data;
            console.log(data);
        });
    };
    HomePage.prototype.test = function () {
        console.log("etst");
    };
    HomePage.prototype.updateDistances = function () {
        /*
        }*/
        this.locations
            .subscribe(function (data) {
            console.log('my data: ', data);
            console.log("DR." + data[0].name);
            console.log("OUR number is", data.length);
            for (var i = 0; i < data.length; i++) {
                //this.distances[i].lat = data[i].lat; 
                //this.distances[i].lon = data[i].lon; 
                //console.log("Lets try: "+ this.urlDistance+data[i].lat+","+data[i].lon);
                //console.log(this.httpClient.get(this.urlDistance+data[i].lat+","+data[i].lon));
            }
            //console.log(this.distances);
        });
    };
    HomePage.prototype.openDetails = function (location) {
        this.navCtrl.push('LocationDetailsPage', { location: location });
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.plt.ready().then(function () {
            var coll = document.getElementsByClassName("item-image");
            var baseimg = 'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';
            var locationInfo = null;
            /*
            if(!locationInfo)
            for(var i=0;i<coll.length;i++){
              locationInfo=this.httpClient.get('http://192.168.0.153:7071/api/Data?app=dc&location='+(i+101));
              locationInfo.subscribe(
                (info) => {
                 // console.log(info);
                  for(var j=0;j<info.length;i++)
                  {if(info[j].data_type=="data:image/png;"){
                    //console.log(atob(baseimg));
                   // baseimg=info[0].data;
                  }}
                //$(coll[i]).attr('src','data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==');
                  
                }
                 )
                 //console.log(atob(baseimg));
                 //console.log(baseimg);
                //$(coll[i]).attr('src','data:image/png;base64,'+baseimg);
                 
            }*/
            /* setTimeout(function(){
              $('.home-slider').slick(
              
              {
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 1,
                dots: true
                
              });
            }, 500);*/
        });
    };
    HomePage.prototype.loadHistoricRoutes = function () {
        var _this = this;
        this.storage.get('routes').then(function (data) {
            if (data) {
                _this.previousTracks = data;
            }
        });
    };
    HomePage.prototype.openTab2 = function () {
        console.log(1);
        //this.app.getActiveNavs()[0].parent.select(2);
        //var t: Tabs = this.navCtrl.parent;
        //t.select(2);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__tab2_tab2__["a" /* Tab2Page */]);
    };
    HomePage.prototype.startTracking = function () {
        var _this = this;
        this.isTracking = true;
        this.trackedRoute = [];
        this.positionSubscription = this.geolocation.watchPosition()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_3_rxjs_operators__["filter"])(function (p) { return p.coords !== undefined; }) //Filter Out Errors
        )
            .subscribe(function (data) {
            setTimeout(function () {
                _this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
                //this.redrawPath(this.trackedRoute);
                //console.log(data.coords.latitude+" - lat"+data.coords.longitude+" - long");
                _this.distances = _this.httpClient.get(_this.urlDistances);
                //this.distances = this.httpClient.get(this.urlDistances+data.coords.latitude+","+data.coords.longitude);
                /*
                this.results = forkJoin(this.locations, this.distances).pipe(
                  map(([locations, distances]) => locations.concat(distances))
              );*/
                var array_length = 0;
                /*
            Observable.combineLatest(this.locations, this.distances).subscribe(
              ([ValOne, ValTwo]) => {
                this.combinedArr = [];
                const length = Math.max(ValOne.length, ValTwo.length);
                for (let i = 0;i < length;i++) {
                  this.combinedArr.push([ValOne[i], ValTwo[i]]);
                  //console.log(ValOne);
                  //console.log(ValOne[i]);
                }
                //array_length++;
                //console.log(this.combinedArr);
              }
            );*/
                //this.results = Observable.combineLatest(this.locations, this.distances);
                /*this.results = combineLatest<any[]>(this.locations, this.distances).pipe(
                  map(arr => arr.reduce((acc, cur) => acc.concat(cur) ) ),
                )*/
                __WEBPACK_IMPORTED_MODULE_6_rxjs__["Observable"].combineLatest(_this.locations, _this.distances).subscribe(function (_a) {
                    /*
                      Example:
                    timerOne first tick: 'Timer One Latest: 1, Timer Two Latest:0, Timer Three Latest: 0
                    timerTwo first tick: 'Timer One Latest: 1, Timer Two Latest:1, Timer Three Latest: 0
                    timerThree first tick: 'Timer One Latest: 1, Timer Two Latest:1, Timer Three Latest: 1
                  */
                    //console.log(timerValOne);
                    //console.log(timerValTwo);
                    var timerValOne = _a[0], timerValTwo = _a[1];
                });
                //console.log ("Results after forkjoin:"+this.results);
                //console.log(this.results);
                //this.results2 = this.locations.concat(this.distances);
                /*
                 this.distances.forEach (element => {
                     element.forEach(distanceInfo => {
                       if(distanceInfo.distance<500)console.log(distanceInfo.distance);
                     });
                 });
         */
                var coll = document.getElementsByClassName("item-image");
                var baseimg = 'iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==';
                var locationInfo = null;
                if (!locationInfo)
                    for (var i = 0; i < coll.length; i++) {
                        //locationInfo=this.httpClient.get('http://192.168.0.153:7071/api/Data?app=dc&location='+(i+101));
                        locationInfo.subscribe(function (info) {
                            //console.log(info);
                            for (var j = 0; j < info.length; i++) {
                                if (info[j].data_type == "data:image/png;") {
                                    console.log(info);
                                    baseimg = info[0].data;
                                }
                            }
                            //$(coll[i]).attr('src','data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==');
                        });
                        //console.log(baseimg);
                        //$(coll[i]).attr('src','data:image/png;base64,'+baseimg);
                    }
                //console.log(this.locations);
                //console.log(this.distances);
                /*
                this.distances.subscribe(
                  x => console.log('Observer got a next value: ' + x[1].distance),
                  err => console.error('Observer got an error: ' + err),
                  () => console.log('Observer got a complete notification')
                );
        
               */
                //check if in polygone
                //console.log((data.coords.latitude+0.001)+" - lat pl 1"+data.coords.longitude+" - long");
                //push a notification start
                /*
                 this.localNotification.requestPermission().then(
                     (permission) => {
                       if (permission === 'granted') {
                   
                         // Create the notification
                         this.localNotification.create('My Title', {
                           tag: 'message1',
                           body: 'My body',
                           icon: 'assets/icon/favicon.ico'
                         });
                   
                       }
                     }
                   );
                   */
                ///let metersLeft = this.calculateDistance(data.coords.latitude, 54.599731,  data.coords.longitude, -5.946327)
                //console.log("METERS LEFT:",metersLeft);
                /*
                if(metersLeft<1000){
                  //this.navCtrl.push('Tab2Page');
                  this.openDetails(this.locations);
                //push a notification
                console.log("You are here!!!!!!!!!!");
                this.localNotification.create('Th2s is the International Wall', {
                  tag: 'geo triggered notification2',
                  body: 'It was build 500000 years ago..',
                  icon: 'assets/icon/tours.ico'
                });
                }
                */
                _this.localNotification.create('Look ahead! This is the International Wall', {
                    tag: 'geo triggered notification',
                    body: 'It was build 100 years ago..',
                    icon: 'assets/icon/tours.ico'
                });
                //
                /*
                           this.push.hasPermission()
                   .then((res: any) => {
       
                     if (res.isEnabled) {
                       console.log('We have permission to send push notifications');
                     } else {
                       console.log('We do not have permission to send push notifications');
                     }
       
                   });*/
                //Create a channel (Android O and above). You'll need to provide the id, description and importance properties.
                /*
                this.push.createChannel({
                id: "testchannel1",
                description: "My first test channel",
                // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
                importance: 3
                }).then(() => console.log('Channel created'));*/
                /*
                          // Delete a channel (Android O and above)
                          this.push.deleteChannel('testchannel1').then(() => console.log('Channel deleted'));
                
                          // Return a list of currently configured channels
                          this.push.listChannels().then((channels) => console.log('List of channels', channels))
                
                          // to initialize push notifications
                
                          const options: PushOptions = {
                            android: {},
                            ios: {
                                alert: 'true',
                                badge: true,
                                sound: 'false'
                            },
                            windows: {},
                            browser: {
                                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
                            }
                          };
                
                          const pushObject: PushObject = this.push.init(options);
                
                
                          pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
                
                          pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
                
                          pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
                          */
                // push a notification end
            }, 0);
        });
    };
    HomePage.prototype.calculateDistance = function (lat1, lat2, long1, long2) {
        var p = 0.017453292519943295; // Math.PI / 180
        var c = Math.cos;
        var a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
        var dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
        return dis * 1000;
    };
    HomePage.prototype.redrawPath = function (path) {
        if (this.currentMapTrack) {
            this.currentMapTrack.setMap(null);
        }
        if (path.length > 1) {
            this.currentMapTrack = new google.maps.Polyline({
                path: path,
                geodesic: true,
                strokeColor: '#ff0000',
                strokeOpacity: 1.0,
                strokeWeight: 6
            });
            this.currentMapTrack.setMap(this.map);
        }
    };
    HomePage.prototype.stopTracking = function () {
        var newRoute = { finished: new Date().getTime(), path: this.trackedRoute };
        this.previousTracks.push(newRoute);
        this.storage.set('routes', this.previousTracks);
        this.isTracking = false;
        this.positionSubscription.unsubscribe();
        this.currentMapTrack.setMap(null);
        this.showHistoryRoute(this.previousTracks);
    };
    HomePage.prototype.showHistoryRoute = function (route) {
        this.redrawPath(route);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/home/home.html"*/'<!-- <ion-header>\n    <ion-navbar color="primary">\n        <ion-title text-center>\n            Places\n        </ion-title>\n    </ion-navbar>\n</ion-header> -->\n\n<ion-content class="card-background-page">\n    <!--\n    <button ion-button full icon-left (click)="startTracking()" *ngIf="!isTracking">\n        <ion-icon name="walk"></ion-icon>\n        Start Tour\n        </button>\n    <button ion-button full color="danger" icon-left (click)="stopTracking()" *ngIf="isTracking">\n      <ion-icon name="hand"></ion-icon>\n      Pause\n  </button>\n\n    <div #map id="map"></div>\n\n    <ion-title margin-top>\n        <ion-icon name="map"></ion-icon> Places you are about to see\n    </ion-title>\n-->\n    <!-- <ion-list>\n        <div class="home-slider">\n            <ion-item text-wrap *ngFor="let location of locations  ">\n\n\n                <ion-thumbnail item-start>\n                    <img class="item-image" src="assets/imgs/wall_thumb.png">\n                </ion-thumbnail>\n                <h2> {{location.name}} </h2>\n                <p text-justify> {{ location.description }} </p>\n                <button ion-button clear item-end> {{ location.distance }}</button>\n\n            </ion-item>\n        </div>\n\n    </ion-list> -->\n\n\n    <div class="home-slider">\n        <div *ngFor="let location of locations  ">\n            <ion-card text-wrap>\n\n                <img src="assets/imgs/wall_thumb.jpg">\n                <div class="card-title">{{location.name}} </div>\n                <div class="card-description">{{ location.description }} </div>\n\n                <!--<button ion-button clear item-end> {{ location.distance }}</button>-->\n\n            </ion-card>\n        </div>\n    </div>\n\n    <button ion-button text-capitalize text-center click="openTab2()">\n            Start Tour\n    </button>\n    <button ion-button click="test()">test</button>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_10__providers_locations_locations__["a" /* LocationsProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_phonegap_local_notification_ngx__["a" /* PhonegapLocalNotification */], __WEBPACK_IMPORTED_MODULE_10__providers_locations_locations__["a" /* LocationsProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabsPage');
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tabs/tabs.html"*/'<!--\n  Generated template for the TabsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>tabs</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tabs/tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NavigatePage = /** @class */ (function () {
    function NavigatePage(navCtrl, navParams, httpClient, geolocation, alertCtrl, locationsProvider, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.geolocation = geolocation;
        this.alertCtrl = alertCtrl;
        this.locationsProvider = locationsProvider;
        this.loadingCtrl = loadingCtrl;
        this.urlDistances = 'assets/distances.json';
        //urlDistances = 'http://192.168.0.153:7071/api/Distances?position=';
        //url = 'http://192.168.0.153:7071/api/Locations';
        this.url = 'assets/locations.json';
        //this.locations = this.httpClient.get(this.url);
        this.myLocation = new google.maps.LatLng(54.6, -5.9);
        this.locations = navParams.get("indexes");
        console.log(this.locations);
        this.directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });
        //this.distances = this.httpClient.get(this.urlDistances+"54.5999349"+","+"-5.99768");
        this.distances = this.httpClient.get(this.urlDistances);
        this.currentPosition = new google.maps.LatLng(54.6, -5.9);
    }
    NavigatePage.prototype.ionViewDidLoad = function () {
        this.loadLocations();
        this.buttonAnimate();
        console.log('ionViewDidLoad NavigatePage');
    };
    NavigatePage.prototype.loadLocations = function () {
        this.loadMap();
        this.destinationCoords = { lat: this.locations[0].lat, lng: this.locations[0].lon };
        this.startTracking();
        /*
            this.locationsProvider.load()
            .then(data => {
              this.locations = data;
              //locations loaded
              this.loadMap();
              this.destinationCoords={lat:this.locations[0].lat, lng: this.locations[0].lon};
              this.startTracking();
              //console.log(this.locations);
            });*/
    };
    NavigatePage.prototype.presentLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    NavigatePage.prototype.buttonAnimate = function () {
        var coll = document.getElementsByClassName("collapsible");
        coll[0].addEventListener("click", function () {
            var content = this.nextElementSibling;
            var icon = $('ion-icon[name="arrow-round-up"]');
            if ($(content).is(':visible')) {
                $(content).slideUp();
                icon.removeClass("ion-md-arrow-round-up");
                icon.addClass("ion-md-arrow-round-down");
            }
            else {
                $(content).slideDown();
                icon.removeClass("ion-md-arrow-round-down");
                icon.addClass("ion-md-arrow-round-up");
            }
        });
    };
    NavigatePage.prototype.startTracking = function () {
        var _this = this;
        this.isTracking = true;
        this.trackedRoute = [];
        // this.startNavigating(this.getCurrentPosition().lat(),this.getCurrentPosition().lng());
        this.positionSubscription = this.geolocation.watchPosition()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["filter"])(function (p) { return p.coords !== undefined; }) //Filter Out Errors
        )
            .subscribe(function (data) {
            setTimeout(function () {
                console.log(data.coords.latitude + " - lat" + data.coords.longitude + " - long");
                //this.currentPosition.lat=data.coords.latitude ;
                //this.currentPosition.lon=data.coords.longitude;
                _this.currentPosition = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
                //console.log(this.locations);
                //this.distances = this.httpClient.get(this.urlDistances+data.coords.latitude+","+data.coords.longitude);
                _this.distances = _this.httpClient.get(_this.urlDistances);
                _this.startNavigating(data.coords.latitude, data.coords.longitude);
            }, 500);
        });
    };
    NavigatePage.prototype.loadMap = function () {
        var latLng = new google.maps.LatLng(54.594443, -5.928693);
        var mapOptions = {
            center: latLng,
            zoom: 15,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "administrative.locality", "elementType": "labels.text.fill", "stylers": [{ "color": "#ff0000" }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }],
            maxZoom: 20,
            minZoom: 0,
            clickableIcons: true,
            draggable: true,
            scrollwheel: true
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.loadMarkers();
    };
    NavigatePage.prototype.startNavigating = function (lat, lon) {
        var _this = this;
        var icons = {
            start: new google.maps.MarkerImage(
            // URL
            'assets/imgs/location-pin-1.png', 
            // (width,height)
            new google.maps.Size(51, 51), 
            // The origin point (x,y)
            new google.maps.Point(0, 0), 
            // The anchor point (x,y)
            new google.maps.Point(22, 32)),
            end: new google.maps.MarkerImage(
            // URL
            'assets/imgs/location-pin-2.png', 
            // (width,height)
            new google.maps.Size(51, 51), 
            // The origin point (x,y)
            new google.maps.Point(0, 0), 
            // The anchor point (x,y)
            new google.maps.Point(22, 32))
        };
        var directionsService = new google.maps.DirectionsService;
        var myloc = new google.maps.Marker({
            clickable: false,
            icon: new google.maps.MarkerImage('//maps.gstatic.com/mapfiles/mobile/mobileimgs2.png', new google.maps.Size(22, 22), new google.maps.Point(0, 18), new google.maps.Point(11, 11)),
            shadow: null,
            zIndex: 999,
            map: this.map
        });
        //console.log(this.locations[this.isItemInArray(this.locations, this.destination)+1].name);
        try {
            //console.log(this.destinationCoords.lat()+ "You are near"+this.locations[this.isItemInArray(this.locations, this.destination)].name);
        }
        catch (_a) { }
        try {
            //console.log("Is it in there? [0, 9]", this.isItemInArray(this.combinedArr, this.destination));   // True
        }
        catch (_b) { }
        //check origin and destination
        //from index of the last one to 
        // Test coor1
        //console.log(this.combinedArr[0].includes(destination));
        //if(this.combinedArr.indexOf(destination)>-1)
        {
            //this destination already exists
            //console.log(this.combinedArr[this.combinedArr.indexOf(destination)+1][0].name+"exists!!!!!");
            //this.destinations[i] = this.combinedArr[i][0].name;
            // destination = this.combinedArr[this.combinedArr.indexOf(destination)+1][0].name;
        }
        //a Shankill Road fix
        // if(this.destination == 'Shankill Road Memorial Garden'){
        //   this.destination = {lat:54.604042, lng: -5.951844};
        // }else if(this.destination == '')
        //console.log("The route is gonna be for:");
        //console.log(this.destinationCoords);
        directionsService.route({
            origin: { lat: lat, lng: lon },
            destination: { lat: this.destinationCoords.lat, lng: this.destinationCoords.lng },
            travelMode: google.maps.TravelMode['WALKING']
        }, function (res, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                _this.directionsDisplay.setMap(_this.map);
                _this.directionsDisplay.setDirections(res);
                _this.directionsDisplay.setPanel(_this.directionsPanel.nativeElement);
                //this.destinationCoords= res.routes[0].legs[0].end_location;
                //console.log(this.calculateDistance( this.destinationCoords.lat, lat, this.destinationCoords.lng, lon));
                if (_this.calculateDistance(_this.destinationCoords.lat, lat, _this.destinationCoords.lng, lon) < 50) {
                    //console.log("!!!!!!!!!!!");
                    //the last one check
                    if (_this.isItemInArray(_this.locations, _this.destinationCoords) < _this.locations.length - 1) {
                        // if(!(typeof this.destination == 'string')){
                        //   this.destination = 'Shankill Road Memorial Garden';
                        // }
                        console.log("You are near" + _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords)].name);
                        _this.currentPosition = { lat: _this.destinationCoords.lat, lng: _this.destinationCoords.lng };
                        _this.destinationCoords = { lat: _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords) + 1].lat, lng: _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords) + 1].lon };
                        _this.startNavigating(_this.currentPosition.lat, _this.currentPosition.lng);
                    }
                    else {
                        _this.navCtrl.push('ThankyouPage');
                    }
                }
                try {
                    _this.startMarker.setMap(null);
                    _this.endMarker.setMap(null);
                }
                catch (_a) { }
                var leg = res.routes[0].legs[0];
                _this.startMarker = _this.makeMarker(leg.start_location, icons.start, "title");
                _this.endMarker = _this.makeMarker(leg.end_location, icons.end, 'title');
            }
            else {
                console.warn(status);
            }
        });
    };
    NavigatePage.prototype.handleLocationError = function (browserHasGeolocation, infoWindow, pos) {
        console.log(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    };
    NavigatePage.prototype.isItemInArray = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            // depends on the format of the array
            if (array[i].lat == item.lat && array[i].lon == item.lng) {
                return i; // Found it
            }
        }
        return -1; // Not found
    };
    NavigatePage.prototype.confirmEndTour = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Do you want to end the tour?',
            message: 'Are you sure you want to stop exploring Belfast?',
            buttons: [
                {
                    text: 'No',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('Agree clicked');
                        _this.navCtrl.push('ThankyouPage');
                    }
                }
            ]
        });
        confirm.present();
    };
    NavigatePage.prototype.getCurrentPosition = function () {
        try {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (data) {
                    //this.myLocation.lat = position.coords.latitude;
                    //this.myLocation.lng = position.coords.longitude;
                    // I just added a marker for you to verify your location
                    // var marker = new google.maps.Marker({
                    //   position: this.myLocation,
                    //   map: this.map
                    // });
                    return new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
                    //this.map.setCenter(this.myLocation);
                }, function () {
                    this.handleLocationError(true, this.map.getCenter());
                });
            }
            else {
                // Browser doesn't support Geolocation
                this.handleLocationError(false, this.map.getCenter(), null);
            }
        }
        catch (_a) { }
        /*
        this.positionSubscription = this.geolocation.watchPosition()
          .pipe(
            filter((p) => p.coords !== undefined) //Filter Out Errors
          )
          .subscribe(data => {
            setTimeout(() => {
    
              console.log(data.coords.latitude, data.coords.longitude);
              return new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
             
              //this.distances = this.httpClient.get(this.urlDistances+data.coords.latitude+","+data.coords.longitude);
            
              
            }, 0);
          });*/
        return new google.maps.LatLng(55, -5);
    };
    NavigatePage.prototype.makeMarker = function (position, icon, title) {
        return new google.maps.Marker({
            position: position,
            map: this.map,
            icon: icon,
            title: title
        });
    };
    NavigatePage.prototype.calculateDistance = function (lat1, lat2, long1, long2) {
        var p = 0.017453292519943295; // Math.PI / 180
        var c = Math.cos;
        var a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
        var dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
        return dis * 1000;
    };
    NavigatePage.prototype.loadMarkers = function () {
        var markerOptions;
        var marker;
        //TODO make it dynamic()
        console.log(this.locations);
        for (var i = 0; i < this.locations.length; i++) {
            //console.log(this.locations[i].lat);
            markerOptions = {
                map: this.map,
                position: {
                    lat: this.locations[i].lat,
                    lng: this.locations[i].lon,
                },
                icon: {
                    path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
                    scale: 1.6363636363636363636363636364,
                    anchor: new google.maps.Point(11, 22),
                    fillOpacity: 1,
                    fillColor: '#be1620',
                    strokeOpacity: 0
                }
            };
            marker = new google.maps.Marker(markerOptions);
        }
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.600136,
        //       lng: -5.941986,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.599693,
        //       lng: -5.946592,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.598649,
        //       lng: -5.949958,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.597989,
        //       lng: -5.952761,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.600409,
        //       lng: -5.949935,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.604042,
        //       lng: -5.951844,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: map,
        //     position: {
        //       lat: 54.603958,
        //       lng: -5.948202,
        //     }
        //   };
        //   markerOptions.icon = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
        // (function(){
        //   var markerOptions = {
        //     map: this.map,
        //     position: {
        //       lat: 54.601535,
        //       lng: -5.947138,
        //     }
        //   };
        //   markerOptions = {
        //     path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
        //     scale: 1.6363636363636363636363636364,
        //     anchor: new google.maps.Point(11, 22),
        //     fillOpacity: 1,
        //     fillColor: '#be1620',
        //     strokeOpacity: 0
        //   };
        //   var marker = new google.maps.Marker(markerOptions);
        // })();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], NavigatePage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('directionsPanel'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], NavigatePage.prototype, "directionsPanel", void 0);
    NavigatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-navigate',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/navigate/navigate.html"*/'<!--\n  Generated template for the NavigatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar hideBackButton="true">\n        <ion-title>Navigate</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-card>\n        <ion-card-content padding-top>\n            <button ion-button full small class="collapsible">\n                <ion-icon name="arrow-round-up"></ion-icon>\n            </button>\n            <div #directionsPanel></div>\n        </ion-card-content>\n    </ion-card>\n    <div #map id="map"></div>\n\n    <button color="dark" ion-button block (click)="confirmEndTour()">\n        End Tour\n    \n    </button>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/navigate/navigate.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__["a" /* LocationsProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__["a" /* LocationsProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], NavigatePage);
    return NavigatePage;
}());

//# sourceMappingURL=navigate.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(359);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pipes_distance_distance__ = __webpack_require__(684);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_phonegap_local_notification_ngx__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tab2_tab2__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_local_notifications_ngx__ = __webpack_require__(685);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_locations_locations__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_tabs_tabs__ = __webpack_require__(352);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tab2_tab2__["a" /* Tab2Page */],
                __WEBPACK_IMPORTED_MODULE_16__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pipes_distance_distance__["a" /* DistancePipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/location-details/location-details.module#LocationDetailsPageModule', name: 'LocationDetailsPage', segment: 'location-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tab1/tab1.module#Tab1PageModule', name: 'Tab1Page', segment: 'tab1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/thankyou/thankyou.module#ThankyouPageModule', name: 'ThankyouPage', segment: 'thankyou', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/navigate/navigate.module#NavigatePageModule', name: 'NavigatePage', segment: 'navigate', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tab2_tab2__["a" /* Tab2Page */],
                __WEBPACK_IMPORTED_MODULE_16__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_local_notifications_ngx__["a" /* LocalNotifications */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_phonegap_local_notification_ngx__["a" /* PhonegapLocalNotification */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_15__providers_locations_locations__["a" /* LocationsProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tab2_tab2__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_navigate_navigate__ = __webpack_require__(353);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, push) {
        this.push = push;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_6__pages_tab2_tab2__["a" /* Tab2Page */];
        this.navigate = __WEBPACK_IMPORTED_MODULE_7__pages_navigate_navigate__["a" /* NavigatePage */];
        platform.ready().then(function () {
            MyApp_1.firebase = fire;
            console.log(MyApp_1.firebase);
            /*console.log(MyApp.firebase.getToken(function(token) {
              // save this server-side and use it to push notifications to this device
              console.log(token);
              console.log("token");
          }, function(error) {
              console.error(error);
              console.error("error");
          }));*/
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp_1 = MyApp;
    MyApp = MyApp_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/app/app.html"*/'<!--- <ion-nav [root]="rootPage"></ion-nav> -->\n<ion-tabs name="myTabsNav">\n    <ion-tab [root]="rootPage" tabIcon="pin"></ion-tab>\n    <ion-tab [root]="tab2Root" tabIcon="walk"></ion-tab>\n    <ion-tab [root]="tab3Root" tabIcon="cog"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__["a" /* Push */]])
    ], MyApp);
    return MyApp;
    var MyApp_1;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 684:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistancePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the DistancePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var DistancePipe = /** @class */ (function () {
    function DistancePipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    DistancePipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (value) {
            return value + ' m';
        }
        else {
            return null;
        }
    };
    DistancePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'distancePipe',
        })
    ], DistancePipe);
    return DistancePipe;
}());

//# sourceMappingURL=distance.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the LocationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LocationsProvider = /** @class */ (function () {
    function LocationsProvider(http) {
        this.http = http;
        this.load();
        console.log('Hello LocationsProvider Provider');
    }
    LocationsProvider.prototype.load = function () {
        var _this = this;
        if (this.data) {
            // already loaded data
            console.log("Already loaded");
            return Promise.resolve(this.data);
        }
        // don't have the data yet
        return new Promise(function (resolve) {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            _this.http.get('http://192.168.0.153:7071/api/Locations?app=dc&state=foreground')
                .subscribe(function (data) {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                _this.data = data;
                resolve(_this.data);
            });
        });
    };
    LocationsProvider.prototype.loadDistances = function (lat, lon) {
        // if (this.distances) {
        //   // already loaded data
        //   console.log("Already loaded");
        //   return Promise.resolve(this.distances);
        // }
        var _this = this;
        // don't have the data yet
        return new Promise(function (resolve) {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            _this.http.get('http://192.168.0.153:7071/api/Distances?position=' + lat + ',' + lon + '&app=dc&state=foreground')
                .subscribe(function (distances) {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                _this.distances = distances;
                resolve(_this.distances);
            });
        });
    };
    LocationsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], LocationsProvider);
    return LocationsProvider;
}());

//# sourceMappingURL=locations.js.map

/***/ })

},[354]);
//# sourceMappingURL=main.js.map