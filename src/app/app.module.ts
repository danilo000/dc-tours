import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Geolocation } from '@ionic-native/geolocation';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { DistancePipe } from '../pipes/distance/distance';
import { Push } from '@ionic-native/push';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification/ngx';
import { Tab2Page } from '../pages/tab2/tab2';
import { SortPipe } from '../pipes/sort/sort';
import { IonPullupModule } from 'ionic-pullup';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { LocationsProvider } from '../providers/locations/locations';
import { TabsPage } from '../pages/tabs/tabs';
import { LocationDetailsPage } from '../pages/location-details/location-details';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Tab2Page,
    TabsPage,
    DistancePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonPullupModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Tab2Page,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    LocalNotifications,
    PhonegapLocalNotification,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Push,
    LocationsProvider
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ]
})
export class AppModule { }
