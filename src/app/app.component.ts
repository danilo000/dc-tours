import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Geolocation } from '@ionic-native/geolocation';

import { HomePage } from '../pages/home/home';
import { LocationDetailsPage } from '../pages/location-details/location-details';
import { Tab2Page } from '../pages/tab2/tab2';
import { NavigatePage } from '../pages/navigate/navigate';
import { LocationsProvider } from '../providers/locations/locations';

declare var fire;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  tab2Root: any = Tab2Page;
  navigate: any = NavigatePage;
  static locationsProvider: any;
  static geoData: any;
  static watch: any;

  static locationsGlobal: any;


  static firebase: any;
  //static locationsProvider: LocationsProvider;

  updateListDistances
  static permissions: any;
  static alert: any;
  static locControl: any;

  static loadLocations() {
    MyApp.locationsProvider.load()
      .then(data => {
        console.log("data:");
        console.log(data);
        MyApp.locationsGlobal = data;
      });
  }
  static onLocationUpdate() {
    //MyApp.updateCardDistance();
    //MyApp.updateLocationDistance();
    MyApp.updateListDistances();
  }

  static async updateListDistances() {
    if (MyApp.geoData != undefined) {
      this.locationsGlobal.forEach(function (element) {
        var htmlElement = document.getElementById("main-list-distance-" + element.id);
        var distance = MyApp.calcDistance(element.latitude, element.longitude, MyApp.geoData.coords.latitude, MyApp.geoData.coords.longitude);
        if (htmlElement != undefined) {
          htmlElement.innerHTML = distance.toString();
        }
      });
    }
  }

  static calcDistance(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    } else {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      return dist;
    }
  }


  static startGeolocation() {
    if (MyApp.geoData == undefined) {
      console.log('subscribe to loc updates');
      MyApp.watch.subscribe((data) => {
        MyApp.geoData = data;
        MyApp.onLocationUpdate();
      });
      MyApp.updateListDistances();
    }
  }

  static checkGeoPermissions() {
    try {
      MyApp.permissions.checkPermission(MyApp.permissions.ACCESS_FINE_LOCATION, function (status) {
        console.log('permission ok');
        if (status.hasPermission) {
          console.log("Yes :D ");
          MyApp.startGeolocation();
        }
        else {
          console.warn("No :( ");
          MyApp.askForGeolocation(false);
        }
      }, async function () {
      });
    } catch (error) {
      MyApp.startGeolocation();
    }
  }


  static async askForGeolocation(focus) {
    const alert = await MyApp.alert.create({
      message: 'Causeway Taste Finder is better with geolocation, do you wish to enable it now?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: maybe later');
          }
        }, {
          text: 'Enable',
          handler: () => {
            console.log('Confirm Okay');
            MyApp.requestGeoPermissions(focus);
          }
        }
      ]
    });

    await alert.present();
  }

  static resetLocControl() {
    MyApp.locControl.stop();
    MyApp.locControl.start();
  }

  static requestGeoPermissions(focus) {
    MyApp.permissions.requestPermission(MyApp.permissions.ACCESS_FINE_LOCATION, function () {
      console.log('permission ok');
      if (focus) {
        MyApp.resetLocControl();
      }
      MyApp.startGeolocation();
    }, async function () {

    });
  }


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private push: Push, public locationsProvider: LocationsProvider, private geolocation: Geolocation) {
    platform.ready().then(() => {

      MyApp.locationsProvider = this.locationsProvider;
      MyApp.watch = this.geolocation.watchPosition();
      MyApp.loadLocations();

      MyApp.firebase = fire;
      console.log(MyApp.firebase);
      setTimeout(() => {
        splashScreen.hide();
      }, 5000);
      statusBar.styleDefault();

      MyApp.checkGeoPermissions();

    });



  }

}

