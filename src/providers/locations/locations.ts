import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { connectableObservableDescriptor } from 'rxjs/observable/ConnectableObservable';

/*
  Generated class for the LocationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()

export class LocationsProvider {
  data :any[];
  distances: any;
  media=[];
  first_index: any;

  constructor(public http: HttpClient) {
    this.load();
    
    console.log('Hello LocationsProvider Provider');
  }

  load() {
    if (this.data) {
      // already loaded data
      console.log("Already loaded");
      return Promise.resolve(this.data);
    }
  
    // don't have the data yet
    return new Promise(resolve => {
      this.data=[];
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      this.http.get('http://192.168.0.153:7071/api/Locations?app=dc&state=foreground')
       
        .subscribe((data : any[]) => {
          this.first_index = data[0].id;
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          
          for(var i=0;i<data.length;i++){
            //console.log(data[i]);//data;
            this.data[i]={
              id:data[i].id,
              description: data[i].description,
              name: data[i].name,
              lat: data[i].lat,
              lon: data[i].lon,
              img: null,
              notification: null,
              distance: null
            }
            this.loadData(i+this.first_index);
            
          }
          //console.log(this.data);
          
          resolve(this.data);
        });
    });
  }

  loadDistances(lat,lon) {
    // if (this.distances) {
    //   // already loaded data
    //   console.log("Already loaded");
    //   return Promise.resolve(this.distances);
    // }
  
    // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      this.http.get('http://192.168.0.153:7071/api/Distances?position='+lat+','+lon+'&app=dc&state=foreground')
       
        .subscribe((distances:any[]) => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          for(var i=0;i<distances.length;i++){
            
            //subscribe distances
              this.data[i].distance=distances[i].distance;
              
            } 
            
          });
          resolve(this.data);
        });
    }

    updateDistances(lat,lon, locations) {
   
      // don't have the data yet
      return new Promise(resolve => {
        // We're using Angular HTTP provider to request the data,
        // then on the response, it'll map the JSON data to a parsed JS object.
        // Next, we process the data and resolve the promise with the new data.
        this.http.get('http://192.168.0.153:7071/api/Distances?position='+lat+','+lon+'&app=dc&state=foreground')
         
          .subscribe((distances:any[]) => {
            // we've got back the raw data, now generate the core schedule data
            // and save the data for later reference
            for(var i=0;i<locations.length;i++){
              
              //subscribe distances
              var id =  locations[i].id;
              if(id>-1)
              locations[i].distance=distances[this.isItemInArray(distances,id)].distance;
              //console.log(locations[i].distance);
              //console.log();
              //this.locations[i].distance = this.distances[this.isItemInArray(this.distances,id)].distance;
            
              } 
              
            });
            resolve(locations);
          });
      }
    
  isItemInArray(array, item) {
      for (var i = 0; i < array.length; i++) {
          // depends on the format of the array
          
          if (array[i].id == item ) {
              return i;   // Found it
          }
      }
      return -1;   // Not found
      }

  //load data of all locations
  loadData(id) {
     if (this.distances) {
    //   // already loaded data
    //   console.log("Already loaded");
       return Promise.resolve(this.distances);
     }
  
    // don't have the data yet
    return new Promise(resolve => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.

      this.http.get('http://192.168.0.153:7071/api/Data?app=dc&location='+id)
       
        .subscribe((data:any[]) => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          var index = id - this.first_index;
          for(var i=0;i<data.length;i++){
            
            //subscribe image
            if(data[i].data_type == "data:image/jpeg;" 
            || data[i].data_type == "data:image/png;" 
            || data[i].data_type == "data:image/jpg;"){
              
              this.data[index].img=data[i].data_type+ data[i].data;
              
            } 
            
          }
          
          resolve(this.data);
        });
    });
  }

}
