import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient } from '@angular/common/http';
import { filter } from 'rxjs/operators';
import { AlertController } from 'ionic-angular';
import { IonPullUpFooterState } from 'ionic-pullup';
import { LocationsProvider } from '../../providers/locations/locations';
import { LocationDetailsPage } from '../location-details/location-details';


/**
 * Generated class for the NavigatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage()
@Component({
  selector: 'page-navigate',
  templateUrl: 'navigate.html',
  providers: [LocationsProvider]
})
export class NavigatePage {



  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  map: any;

  footerState: IonPullUpFooterState;
  currentPosition: any;
  locations: any[];
  distances: Observable<any>;
  directionsDisplay: any;

  isTracking: boolean;
  trackedRoute: any[];
  positionSubscription: any;
  currentLocMarker: any;
  startMarker: any;
  endMarker: any;
  destinationCoords: any;

  myLocation: any;
  loading: any;
  icons: any = {
    start: new google.maps.MarkerImage(
      // URL
      'assets/imgs/location-pin-1.png',
      // (width,height)
      new google.maps.Size(51, 51),
      // The origin point (x,y)
      new google.maps.Point(0, 0),
      // The anchor point (x,y)
      new google.maps.Point(22, 32)),
    end: new google.maps.MarkerImage(
      // URL
      'assets/imgs/location-pin-2.png',
      // (width,height)
      new google.maps.Size(51, 51),
      // The origin point (x,y)
      new google.maps.Point(0, 0),
      // The anchor point (x,y)
      new google.maps.Point(22, 32)
    )
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient, public modalCtrl: ModalController,
    public geolocation: Geolocation, public alertCtrl: AlertController, public locationsProvider: LocationsProvider, public loadingCtrl: LoadingController) {

    this.footerState = IonPullUpFooterState.Collapsed;

    this.locations = navParams.get("indexes");
    console.log(this.locations);

    this.directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });

  }

  // popupDetails(location) {
  //   let detailsModal = this.modalCtrl.create(LocationDetailsPage, location);
  //   detailsModal.present();
  // }


  ionViewDidLoad() {

    this.loadLocations();

    this.buttonAnimate();

    console.log('ionViewDidLoad NavigatePage');


  }

  loadLocations() {

    this.loadMap();
    this.destinationCoords = { lat: this.locations[0].lat, lng: this.locations[0].lon };
    this.startTracking();

  }


  presentLoading() {

    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    this.loading.present();

  }

  buttonAnimate() {

    var coll = document.getElementsByClassName("collapsible");

    coll[0].addEventListener("click", function () {

      var content = this.nextElementSibling;

      var icon = $('ion-icon[name="arrow-round-up"]');

      if ($(content).is(':visible')) {
        $(content).slideUp();

        icon.removeClass("ion-md-arrow-round-up");
        icon.addClass("ion-md-arrow-round-down");
      } else {
        $(content).slideDown();

        icon.removeClass("ion-md-arrow-round-down");
        icon.addClass("ion-md-arrow-round-up");
      }
    });
  }
  reachedStop: boolean = true;
  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];

    var icons = {
      circle: new google.maps.MarkerImage(
        // URL
        'https://ssl.gstatic.com/gb/images/spinner_32.gif',
        // (width,height)
        new google.maps.Size(32, 32),
        // The origin point (x,y)
        new google.maps.Point(0, 0),
        // The anchor point (x,y)
        new google.maps.Point(22, 32))
    };
    this.positionSubscription = this.geolocation.watchPosition()
      .pipe(
        filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {

          console.log(data.coords.latitude + " - lat" + data.coords.longitude + " - long");
          this.currentPosition = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);

          try {
            this.currentLocMarker.setMap(null);

          }
          catch { }

          this.currentLocMarker = this.makeMarker(this.currentPosition, icons.circle, "title");


          if (this.reachedStop) {
            this.startNavigating(data.coords.latitude, data.coords.longitude);
            this.reachedStop = false;
          }

          console.log(this.calculateDistance(this.destinationCoords.lat, data.coords.latitude, this.destinationCoords.lng, data.coords.longitude));
          if (this.calculateDistance(this.destinationCoords.lat, data.coords.latitude, this.destinationCoords.lng, data.coords.longitude) < 50) {
            this.reachedStop = true;
            if (this.isItemInArray(this.locations, this.destinationCoords) < this.locations.length - 1) {

              console.log("You are near" + this.locations[this.isItemInArray(this.locations, this.destinationCoords)].name);
              this.navCtrl.push(LocationDetailsPage, this.locations[this.isItemInArray(this.locations, this.destinationCoords)]);
              this.currentPosition = { lat: this.destinationCoords.lat, lng: this.destinationCoords.lng };
              this.destinationCoords = { lat: this.locations[this.isItemInArray(this.locations, this.destinationCoords) + 1].lat, lng: this.locations[this.isItemInArray(this.locations, this.destinationCoords) + 1].lon };
              this.startNavigating(this.currentPosition.lat, this.currentPosition.lng);
            }
            else {
              this.navCtrl.push('ThankyouPage');
            }

          }
        }, 100);
      });

  }
  loadMap() {

    let latLng = new google.maps.LatLng(54.594443, -5.928693);


    let mapOptions = {
      center: latLng,
      zoom: 15,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "administrative.locality", "elementType": "labels.text.fill", "stylers": [{ "color": "#ff0000" }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }],
      maxZoom: 20,
      minZoom: 0,
      clickableIcons: true,
      draggable: true,
      scrollwheel: true

    }


    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.loadMarkers();

  }

  startNavigating(lat, lon) {


    let directionsService = new google.maps.DirectionsService;


    directionsService.route({
      origin: { lat: lat, lng: lon },
      destination: { lat: this.destinationCoords.lat, lng: this.destinationCoords.lng }, //typeof this.destination=='string'?this.destination+" Belfast":{lat:54.604042, lng: -5.951844},
      travelMode: google.maps.TravelMode['WALKING']
    }, (res, status) => {

      if (status == google.maps.DirectionsStatus.OK) {

        //only when it starts

        this.directionsDisplay.setDirections(res);
        this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);
        this.directionsDisplay.setMap(this.map);

        try {
          this.startMarker.setMap(null);
          this.endMarker.setMap(null);
        }
        catch { }
        var leg = res.routes[0].legs[0];
        this.startMarker = this.makeMarker(leg.start_location, this.icons.start, "title");
        this.endMarker = this.makeMarker(leg.end_location, this.icons.end, 'title');

      } else {
        console.warn(status);
      }

    });

  }

  handleLocationError(browserHasGeolocation, infoWindow, pos) {
    console.log(browserHasGeolocation ?
      'Error: The Geolocation service failed.' :
      'Error: Your browser doesn\'t support geolocation.');
  }

  isItemInArray(array, item) {
    for (var i = 0; i < array.length; i++) {
      // depends on the format of the array

      if (array[i].lat == item.lat && array[i].lon == item.lng) {
        return i;   // Found it
      }
    }
    return -1;   // Not found
  }

  confirmEndTour() {
    const confirm = this.alertCtrl.create({
      title: 'Do you want to end the tour?',
      message: 'Are you sure you want to stop exploring Belfast?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.navCtrl.push('ThankyouPage');
          }
        }
      ]
    });
    confirm.present();

  }
  /*
    getCurrentPosition() {
  
      try {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function (data) {
  
            return new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
  
          }, function () {
            this.handleLocationError(true, this.map.getCenter());
          });
        } else {
          // Browser doesn't support Geolocation
          this.handleLocationError(false, this.map.getCenter(), null);
        }
      } catch{ }
    
      return new google.maps.LatLng(55, -5);
    }
  */
  makeMarker(position, icon, title) {
    return new google.maps.Marker({
      position: position,
      map: this.map,
      icon: icon,
      title: title,
      optimized: false
    });
  }

  calculateDistance(lat1: number, lat2: number, long1: number, long2: number) {
    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    return dis * 1000;
  }

  loadMarkers() {
    var markerOptions;
    var marker;

    console.log(this.locations);
    for (var i = 0; i < this.locations.length; i++) {
      markerOptions = {
        map: this.map,
        position: {
          lat: this.locations[i].lat,
          lng: this.locations[i].lon,
        },
        icon: {
          path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
          scale: 1.6363636363636363636363636364,
          anchor: new google.maps.Point(11, 22),
          fillOpacity: 1,
          fillColor: '#be1620',
          strokeOpacity: 0
        }
      };
      marker = new google.maps.Marker(markerOptions);
    }

  }

  footerExpanded() {
    console.log('Footer expanded!');
  }

  footerCollapsed() {
    console.log('Footer collapsed!');
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
  }

}
