import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import { filter, delay } from 'rxjs/operators';
import { LocationsProvider } from '../../providers/locations/locations';
import { MyApp } from '../../app/app.component';
/**
 * Generated class for the Tab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-tab2',
  templateUrl: 'tab2.html',
})
export class Tab2Page {

  locations: any;
  items = [];
  positionSubscription: any;
  isTracking: boolean;
  trackedRoute: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpClient: HttpClient,
    public geolocation: Geolocation, public locationsProvider: LocationsProvider) {

    this.startTracking();

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Tab2Page');

    MyApp.updateListDistances();

  }



  reorderItems(indexes) {

    let element = this.locations[indexes.from];
    this.locations.splice(indexes.from, 1);
    this.locations.splice(indexes.to, 0, element);

  }

  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];

    this.positionSubscription = this.geolocation.watchPosition()
      .pipe(
        filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {

          console.log("Current location: " + data.coords.latitude + ", " + data.coords.longitude);
          this.loadDistances(data.coords.latitude, data.coords.longitude);

        }, 2000);
      });

  }

  start(page) {
    console.log("start");

    //filter
    var locs = [];
    /* for (let i = 0; i < this.combinedArr.length; i++) {
       locs[i]=
         {
           id: this.locations[i].id,
           name: this.locations[i].name,
           distance: this.locations[i].distance,
           lat: this.locations[i].lat,
           lon: this.locations[i].lon
         };
     }*/

    //console.log(locs.length)


    this.navCtrl.push('NavigatePage', { indexes: this.locations });
  }

  sortList() {

    try {
      let count = this.locations.length;
      let swapped;

      for (var i = 0; i < count; i++) {
        swapped = false;
        for (var j = 0; j < count - 1; j++) { //j=i
          if (this.locations[i].distance < this.locations[j].distance) {

            this.reorderItems({ from: i, to: j });
            swapped = true;
          }
        }

        if (!swapped) {
          break;
        }
      }
    }
    catch{ }
  }

  ionViewWillEnter() {
    this.locations = MyApp.locationsGlobal;

    console.log(this.locations);
  }

  loadDistances(lat, lon) {

    if (!this.locations)
      this.locationsProvider.loadDistances(lat, lon)
        .then(data => {


          this.locations = data;

        });
    else
      this.locationsProvider.updateDistances(lat, lon, this.locations)
        .then(data => {
          //console.log("updated");

          this.locations = data;

        });


  }

  deleteItem(item) {

    //TODO: write the value to an array for further check

    for (let i = 0; i < this.locations.length; i++) {
      if (this.locations[i] == item) {

        this.locations.splice(i, 1);
      }
    }
  }
}
