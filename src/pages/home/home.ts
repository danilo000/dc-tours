import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs/Subscription';
import { filter, map, combineLatest, concat, delay, merge } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { mapTo } from 'rxjs/operators';
import { PhonegapLocalNotification } from '@ionic-native/phonegap-local-notification/ngx';
import { MyApp } from '../../app/app.component';
import * as $ from "jquery";
import "slick-carousel";
import { LocationsProvider } from '../../providers/locations/locations';

declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [LocationsProvider]
})
export class HomePage {


  map: any;
  currentMapTrack = null;

  isTracking = false;
  trackedRoute = [];
  previousTracks = [];

  positionSubscription: Subscription;
  distances: Observable<any>;
  results: any;
  results2: any;
  combinedArr: any;
  locations: any;

  constructor(public navCtrl: NavController, private plt: Platform, private geolocation: Geolocation, private storage: Storage,
    private push: Push, public httpClient: HttpClient, private localNotification: PhonegapLocalNotification, public locationsProvider: LocationsProvider) {

  }

  openDetails(location) {
    this.navCtrl.push('LocationDetailsPage', { location: location });
  }

  ionViewWillEnter() {
    this.locations = MyApp.locationsGlobal;
    console.log(this.locations);
  }

  ionViewDidLoad() {
    this.plt.ready().then(() => {
      var coll = document.getElementsByClassName("item-image");

      setTimeout(function () {
        $('.home-slider').slick(

          {
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 1,
            dots: true

          });
      }, 600);

      //test notificaition
      this.localNotification.create('Look ahead! This is the International Wall', {
        tag: 'geo triggered notification',
        body: 'It was build 100 years ago..',
        icon: 'assets/icon/tours.ico'
      });
    });
  }

  openTab2() {
    //not the best solution, but works
    //tried using .push or .setRoot but then the active tab icon doesn't change
    document.getElementById('tab-t0-1').click();
  }


}