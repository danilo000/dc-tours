import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LocationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-location-details',
  templateUrl: 'location-details.html',
})
export class LocationDetailsPage {

  location: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.location = this.navParams.get('location');
    console.log("here");
    console.log(this.location);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocationDetailsPage');
  }

}
