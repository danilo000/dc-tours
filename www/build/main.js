webpackJsonp([6],{

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tab2_tab2__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_navigate_navigate__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_locations_locations__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, push, locationsProvider, geolocation) {
        var _this = this;
        this.push = push;
        this.locationsProvider = locationsProvider;
        this.geolocation = geolocation;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_7__pages_tab2_tab2__["a" /* Tab2Page */];
        this.navigate = __WEBPACK_IMPORTED_MODULE_8__pages_navigate_navigate__["a" /* NavigatePage */];
        platform.ready().then(function () {
            MyApp_1.locationsProvider = _this.locationsProvider;
            MyApp_1.watch = _this.geolocation.watchPosition();
            MyApp_1.loadLocations();
            MyApp_1.firebase = fire;
            console.log(MyApp_1.firebase);
            setTimeout(function () {
                splashScreen.hide();
            }, 5000);
            statusBar.styleDefault();
            MyApp_1.checkGeoPermissions();
        });
    }
    MyApp_1 = MyApp;
    MyApp.loadLocations = function () {
        MyApp_1.locationsProvider.load()
            .then(function (data) {
            console.log("data:");
            console.log(data);
            MyApp_1.locationsGlobal = data;
        });
    };
    MyApp.onLocationUpdate = function () {
        //MyApp.updateCardDistance();
        //MyApp.updateLocationDistance();
        MyApp_1.updateListDistances();
    };
    MyApp.updateListDistances = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (MyApp_1.geoData != undefined) {
                    this.locationsGlobal.forEach(function (element) {
                        var htmlElement = document.getElementById("main-list-distance-" + element.id);
                        var distance = MyApp_1.calcDistance(element.latitude, element.longitude, MyApp_1.geoData.coords.latitude, MyApp_1.geoData.coords.longitude);
                        if (htmlElement != undefined) {
                            htmlElement.innerHTML = distance.toString();
                        }
                    });
                }
                return [2 /*return*/];
            });
        });
    };
    MyApp.calcDistance = function (lat1, lon1, lat2, lon2) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            var radlat1 = Math.PI * lat1 / 180;
            var radlat2 = Math.PI * lat2 / 180;
            var theta = lon1 - lon2;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            return dist;
        }
    };
    MyApp.startGeolocation = function () {
        if (MyApp_1.geoData == undefined) {
            console.log('subscribe to loc updates');
            MyApp_1.watch.subscribe(function (data) {
                MyApp_1.geoData = data;
                MyApp_1.onLocationUpdate();
            });
            MyApp_1.updateListDistances();
        }
    };
    MyApp.checkGeoPermissions = function () {
        try {
            MyApp_1.permissions.checkPermission(MyApp_1.permissions.ACCESS_FINE_LOCATION, function (status) {
                console.log('permission ok');
                if (status.hasPermission) {
                    console.log("Yes :D ");
                    MyApp_1.startGeolocation();
                }
                else {
                    console.warn("No :( ");
                    MyApp_1.askForGeolocation(false);
                }
            }, function () {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        return [2 /*return*/];
                    });
                });
            });
        }
        catch (error) {
            MyApp_1.startGeolocation();
        }
    };
    MyApp.askForGeolocation = function (focus) {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, MyApp_1.alert.create({
                            message: 'Causeway Taste Finder is better with geolocation, do you wish to enable it now?',
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel: maybe later');
                                    }
                                }, {
                                    text: 'Enable',
                                    handler: function () {
                                        console.log('Confirm Okay');
                                        MyApp_1.requestGeoPermissions(focus);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    MyApp.resetLocControl = function () {
        MyApp_1.locControl.stop();
        MyApp_1.locControl.start();
    };
    MyApp.requestGeoPermissions = function (focus) {
        MyApp_1.permissions.requestPermission(MyApp_1.permissions.ACCESS_FINE_LOCATION, function () {
            console.log('permission ok');
            if (focus) {
                MyApp_1.resetLocControl();
            }
            MyApp_1.startGeolocation();
        }, function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/];
                });
            });
        });
    };
    MyApp = MyApp_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/app/app.html"*/'<ion-tabs>\n    <ion-tab [root]="rootPage" tabIcon="pin"></ion-tab>\n    <ion-tab [root]="tab2Root" tabIcon="walk"></ion-tab>\n    <ion-tab [root]="tab3Root" tabIcon="cog"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__["a" /* Push */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__["a" /* Push */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_9__providers_locations_locations__["a" /* LocationsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_9__providers_locations_locations__["a" /* LocationsProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */]) === "function" && _f || Object])
    ], MyApp);
    return MyApp;
    var MyApp_1, _a, _b, _c, _d, _e, _f;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 166;

/***/ }),

/***/ 210:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/location-details/location-details.module": [
		691,
		5
	],
	"../pages/navigate/navigate.module": [
		696,
		4
	],
	"../pages/tab1/tab1.module": [
		692,
		2
	],
	"../pages/tabs/tabs.module": [
		693,
		3
	],
	"../pages/thankyou/thankyou.module": [
		694,
		1
	],
	"../pages/welcome/welcome.module": [
		695,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 210;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_phonegap_local_notification_ngx__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_component__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_jquery__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_slick_carousel__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_slick_carousel___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_slick_carousel__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_locations_locations__ = __webpack_require__(66);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, plt, geolocation, storage, push, httpClient, localNotification, locationsProvider) {
        this.navCtrl = navCtrl;
        this.plt = plt;
        this.geolocation = geolocation;
        this.storage = storage;
        this.push = push;
        this.httpClient = httpClient;
        this.localNotification = localNotification;
        this.locationsProvider = locationsProvider;
        this.currentMapTrack = null;
        this.isTracking = false;
        this.trackedRoute = [];
        this.previousTracks = [];
    }
    HomePage.prototype.openDetails = function (location) {
        this.navCtrl.push('LocationDetailsPage', { location: location });
    };
    HomePage.prototype.ionViewWillEnter = function () {
        this.locations = __WEBPACK_IMPORTED_MODULE_7__app_app_component__["a" /* MyApp */].locationsGlobal;
        console.log(this.locations);
    };
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.plt.ready().then(function () {
            var coll = document.getElementsByClassName("item-image");
            setTimeout(function () {
                __WEBPACK_IMPORTED_MODULE_8_jquery__('.home-slider').slick({
                    centerMode: true,
                    centerPadding: '60px',
                    slidesToShow: 1,
                    dots: true
                });
            }, 600);
            //test notificaition
            _this.localNotification.create('Look ahead! This is the International Wall', {
                tag: 'geo triggered notification',
                body: 'It was build 100 years ago..',
                icon: 'assets/icon/tours.ico'
            });
        });
    };
    HomePage.prototype.openTab2 = function () {
        //not the best solution, but works
        //tried using .push or .setRoot but then the active tab icon doesn't change
        document.getElementById('tab-t0-1').click();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/home/home.html"*/'<ion-content class="card-background-page">\n\n\n\n    <div class="home-slider">\n        <ion-card text-wrap>\n\n            <img src="assets/imgs/welcome.png" style="min-height: 100%;">\n            <div class="card-title">Get started </div>\n            <div class="card-description">Swipe through the historical locations that you can visit on your walking tour today!</div>\n\n        </ion-card>\n        <div *ngFor="let location of locations  ">\n            <ion-card text-wrap class="card-with-image">\n\n                <img src="{{location.img}}">\n                <div class=" card-title ">{{location.name}} </div>\n                <div class="card-description ">{{ location.description }} </div>\n\n                <!--<button ion-button clear item-end> {{ location.distance }}</button>-->\n\n            </ion-card>\n        </div>\n    </div>\n\n    <button round ion-button text-center (click)="openTab2() ">\n            START TOUR\n    </button>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_10__providers_locations_locations__["a" /* LocationsProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_push__["a" /* Push */], __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_phonegap_local_notification_ngx__["a" /* PhonegapLocalNotification */], __WEBPACK_IMPORTED_MODULE_10__providers_locations_locations__["a" /* LocationsProvider */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 347:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tab2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_component__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the Tab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Tab2Page = /** @class */ (function () {
    function Tab2Page(navCtrl, navParams, httpClient, geolocation, locationsProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.geolocation = geolocation;
        this.locationsProvider = locationsProvider;
        this.items = [];
        this.startTracking();
    }
    Tab2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Tab2Page');
        __WEBPACK_IMPORTED_MODULE_6__app_app_component__["a" /* MyApp */].updateListDistances();
    };
    Tab2Page.prototype.reorderItems = function (indexes) {
        var element = this.locations[indexes.from];
        this.locations.splice(indexes.from, 1);
        this.locations.splice(indexes.to, 0, element);
    };
    Tab2Page.prototype.startTracking = function () {
        var _this = this;
        this.isTracking = true;
        this.trackedRoute = [];
        this.positionSubscription = this.geolocation.watchPosition()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["filter"])(function (p) { return p.coords !== undefined; }) //Filter Out Errors
        )
            .subscribe(function (data) {
            setTimeout(function () {
                console.log("Current location: " + data.coords.latitude + ", " + data.coords.longitude);
                _this.loadDistances(data.coords.latitude, data.coords.longitude);
            }, 2000);
        });
    };
    Tab2Page.prototype.start = function (page) {
        console.log("start");
        //filter
        var locs = [];
        /* for (let i = 0; i < this.combinedArr.length; i++) {
           locs[i]=
             {
               id: this.locations[i].id,
               name: this.locations[i].name,
               distance: this.locations[i].distance,
               lat: this.locations[i].lat,
               lon: this.locations[i].lon
             };
         }*/
        //console.log(locs.length)
        this.navCtrl.push('NavigatePage', { indexes: this.locations });
    };
    Tab2Page.prototype.sortList = function () {
        try {
            var count = this.locations.length;
            var swapped = void 0;
            for (var i = 0; i < count; i++) {
                swapped = false;
                for (var j = 0; j < count - 1; j++) {
                    if (this.locations[i].distance < this.locations[j].distance) {
                        this.reorderItems({ from: i, to: j });
                        swapped = true;
                    }
                }
                if (!swapped) {
                    break;
                }
            }
        }
        catch (_a) { }
    };
    Tab2Page.prototype.ionViewWillEnter = function () {
        this.locations = __WEBPACK_IMPORTED_MODULE_6__app_app_component__["a" /* MyApp */].locationsGlobal;
        console.log(this.locations);
    };
    Tab2Page.prototype.loadDistances = function (lat, lon) {
        var _this = this;
        if (!this.locations)
            this.locationsProvider.loadDistances(lat, lon)
                .then(function (data) {
                _this.locations = data;
            });
        else
            this.locationsProvider.updateDistances(lat, lon, this.locations)
                .then(function (data) {
                //console.log("updated");
                _this.locations = data;
            });
    };
    Tab2Page.prototype.deleteItem = function (item) {
        //TODO: write the value to an array for further check
        for (var i = 0; i < this.locations.length; i++) {
            if (this.locations[i] == item) {
                this.locations.splice(i, 1);
            }
        }
    };
    Tab2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-tab2',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tab2/tab2.html"*/'<!--\n  Generated template for the Tab2Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content>\n\n    <ion-title text-wrap margin>Reorder and Amend the list of locations you would like to visit. </ion-title>\n\n    <!-- <ion-item margin-top>\n        <ion-label>\n            <ion-icon name="locate"></ion-icon>\n            Your current location\n            <h3 text-right> 0 m </h3>\n\n        </ion-label>\n\n    </ion-item> \n-->\n    <button round ion-button class="sort-btn" (click)="sortList()">Present an ideal path</button>\n\n    <ion-list no-lines reorder="true" (ionItemReorder)="reorderItems($event)">\n\n        <ion-item-sliding *ngFor="let location of locations">\n\n            <ion-item>\n\n                <ion-label>\n\n                    <ion-card text-wrap class="card-with-image">\n\n                        <img src="{{location.img}}">\n                        <div text-wrap class=" card-title ">{{location.name}} </div>\n                        <ion-badge>\n                            <div class="card-description" text-right>{{ location.distance | number:\'1.0-0\' | distancePipe }} </div>\n                        </ion-badge>\n                        <!--<button ion-button clear item-end> {{ location.distance }}</button>-->\n\n                    </ion-card>\n\n                </ion-label>\n\n\n            </ion-item>\n\n\n\n            <ion-item-options side="right">\n                <button ion-button color="danger" (click)="deleteItem(location)">\n                    <ion-icon name="trash"></ion-icon>\n                    Remove\n                </button>\n            </ion-item-options>\n\n\n        </ion-item-sliding>\n\n    </ion-list>\n\n    <ion-fab center bottom>\n\n        <button round ion-button class="start-btn" icon-end (click)="start(location)">\n            Start Tour\n            \n        </button>\n\n    </ion-fab>\n\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tab2/tab2.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__["a" /* LocationsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_locations_locations__["a" /* LocationsProvider */]) === "function" && _e || Object])
    ], Tab2Page);
    return Tab2Page;
    var _a, _b, _c, _d, _e;
}());

//# sourceMappingURL=tab2.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LocationDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LocationDetailsPage = /** @class */ (function () {
    function LocationDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.location = this.navParams.get('location');
        console.log("here");
        console.log(this.location);
    }
    LocationDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LocationDetailsPage');
    };
    LocationDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-location-details',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/location-details/location-details.html"*/'<!--\n  Generated template for the LocationDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-header>\n    <ion-navbar color="primary">\n        <ion-title></ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content fullscreen>\n\n    <ion-card>\n\n        <ion-item *ngFor="let details of location">\n            <p> {{details.name}} </p>\n        </ion-item>\n\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/location-details/location-details.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _b || Object])
    ], LocationDetailsPage);
    return LocationDetailsPage;
    var _a, _b;
}());

//# sourceMappingURL=location-details.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TabsPage');
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-tabs',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tabs/tabs.html"*/'<!--\n  Generated template for the TabsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>tabs</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/tabs/tabs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_operators___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_locations_locations__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__location_details_location_details__ = __webpack_require__(356);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var NavigatePage = /** @class */ (function () {
    function NavigatePage(navCtrl, navParams, httpClient, modalCtrl, geolocation, alertCtrl, locationsProvider, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpClient = httpClient;
        this.modalCtrl = modalCtrl;
        this.geolocation = geolocation;
        this.alertCtrl = alertCtrl;
        this.locationsProvider = locationsProvider;
        this.loadingCtrl = loadingCtrl;
        this.icons = {
            start: new google.maps.MarkerImage(
            // URL
            'assets/imgs/location-pin-1.png', 
            // (width,height)
            new google.maps.Size(51, 51), 
            // The origin point (x,y)
            new google.maps.Point(0, 0), 
            // The anchor point (x,y)
            new google.maps.Point(22, 32)),
            end: new google.maps.MarkerImage(
            // URL
            'assets/imgs/location-pin-2.png', 
            // (width,height)
            new google.maps.Size(51, 51), 
            // The origin point (x,y)
            new google.maps.Point(0, 0), 
            // The anchor point (x,y)
            new google.maps.Point(22, 32))
        };
        this.reachedStop = true;
        this.footerState = __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
        this.locations = navParams.get("indexes");
        console.log(this.locations);
        this.directionsDisplay = new google.maps.DirectionsRenderer({ suppressMarkers: true });
    }
    // popupDetails(location) {
    //   let detailsModal = this.modalCtrl.create(LocationDetailsPage, location);
    //   detailsModal.present();
    // }
    NavigatePage.prototype.ionViewDidLoad = function () {
        this.loadLocations();
        this.buttonAnimate();
        console.log('ionViewDidLoad NavigatePage');
    };
    NavigatePage.prototype.loadLocations = function () {
        this.loadMap();
        this.destinationCoords = { lat: this.locations[0].lat, lng: this.locations[0].lon };
        this.startTracking();
    };
    NavigatePage.prototype.presentLoading = function () {
        this.loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        this.loading.present();
    };
    NavigatePage.prototype.buttonAnimate = function () {
        var coll = document.getElementsByClassName("collapsible");
        coll[0].addEventListener("click", function () {
            var content = this.nextElementSibling;
            var icon = $('ion-icon[name="arrow-round-up"]');
            if ($(content).is(':visible')) {
                $(content).slideUp();
                icon.removeClass("ion-md-arrow-round-up");
                icon.addClass("ion-md-arrow-round-down");
            }
            else {
                $(content).slideDown();
                icon.removeClass("ion-md-arrow-round-down");
                icon.addClass("ion-md-arrow-round-up");
            }
        });
    };
    NavigatePage.prototype.startTracking = function () {
        var _this = this;
        this.isTracking = true;
        this.trackedRoute = [];
        var icons = {
            circle: new google.maps.MarkerImage(
            // URL
            'https://ssl.gstatic.com/gb/images/spinner_32.gif', 
            // (width,height)
            new google.maps.Size(32, 32), 
            // The origin point (x,y)
            new google.maps.Point(0, 0), 
            // The anchor point (x,y)
            new google.maps.Point(22, 32))
        };
        this.positionSubscription = this.geolocation.watchPosition()
            .pipe(Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_operators__["filter"])(function (p) { return p.coords !== undefined; }) //Filter Out Errors
        )
            .subscribe(function (data) {
            setTimeout(function () {
                console.log(data.coords.latitude + " - lat" + data.coords.longitude + " - long");
                _this.currentPosition = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
                try {
                    _this.currentLocMarker.setMap(null);
                }
                catch (_a) { }
                _this.currentLocMarker = _this.makeMarker(_this.currentPosition, icons.circle, "title");
                if (_this.reachedStop) {
                    _this.startNavigating(data.coords.latitude, data.coords.longitude);
                    _this.reachedStop = false;
                }
                console.log(_this.calculateDistance(_this.destinationCoords.lat, data.coords.latitude, _this.destinationCoords.lng, data.coords.longitude));
                if (_this.calculateDistance(_this.destinationCoords.lat, data.coords.latitude, _this.destinationCoords.lng, data.coords.longitude) < 50) {
                    _this.reachedStop = true;
                    if (_this.isItemInArray(_this.locations, _this.destinationCoords) < _this.locations.length - 1) {
                        console.log("You are near" + _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords)].name);
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__location_details_location_details__["a" /* LocationDetailsPage */], _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords)]);
                        _this.currentPosition = { lat: _this.destinationCoords.lat, lng: _this.destinationCoords.lng };
                        _this.destinationCoords = { lat: _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords) + 1].lat, lng: _this.locations[_this.isItemInArray(_this.locations, _this.destinationCoords) + 1].lon };
                        _this.startNavigating(_this.currentPosition.lat, _this.currentPosition.lng);
                    }
                    else {
                        _this.navCtrl.push('ThankyouPage');
                    }
                }
            }, 100);
        });
    };
    NavigatePage.prototype.loadMap = function () {
        var latLng = new google.maps.LatLng(54.594443, -5.928693);
        var mapOptions = {
            center: latLng,
            zoom: 15,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "administrative.locality", "elementType": "labels.text.fill", "stylers": [{ "color": "#ff0000" }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }],
            maxZoom: 20,
            minZoom: 0,
            clickableIcons: true,
            draggable: true,
            scrollwheel: true
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.loadMarkers();
    };
    NavigatePage.prototype.startNavigating = function (lat, lon) {
        var _this = this;
        var directionsService = new google.maps.DirectionsService;
        directionsService.route({
            origin: { lat: lat, lng: lon },
            destination: { lat: this.destinationCoords.lat, lng: this.destinationCoords.lng },
            travelMode: google.maps.TravelMode['WALKING']
        }, function (res, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                //only when it starts
                _this.directionsDisplay.setDirections(res);
                _this.directionsDisplay.setPanel(_this.directionsPanel.nativeElement);
                _this.directionsDisplay.setMap(_this.map);
                try {
                    _this.startMarker.setMap(null);
                    _this.endMarker.setMap(null);
                }
                catch (_a) { }
                var leg = res.routes[0].legs[0];
                _this.startMarker = _this.makeMarker(leg.start_location, _this.icons.start, "title");
                _this.endMarker = _this.makeMarker(leg.end_location, _this.icons.end, 'title');
            }
            else {
                console.warn(status);
            }
        });
    };
    NavigatePage.prototype.handleLocationError = function (browserHasGeolocation, infoWindow, pos) {
        console.log(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    };
    NavigatePage.prototype.isItemInArray = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            // depends on the format of the array
            if (array[i].lat == item.lat && array[i].lon == item.lng) {
                return i; // Found it
            }
        }
        return -1; // Not found
    };
    NavigatePage.prototype.confirmEndTour = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Do you want to end the tour?',
            message: 'Are you sure you want to stop exploring Belfast?',
            buttons: [
                {
                    text: 'No',
                    handler: function () {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('Agree clicked');
                        _this.navCtrl.push('ThankyouPage');
                    }
                }
            ]
        });
        confirm.present();
    };
    /*
      getCurrentPosition() {
    
        try {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (data) {
    
              return new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
    
            }, function () {
              this.handleLocationError(true, this.map.getCenter());
            });
          } else {
            // Browser doesn't support Geolocation
            this.handleLocationError(false, this.map.getCenter(), null);
          }
        } catch{ }
      
        return new google.maps.LatLng(55, -5);
      }
    */
    NavigatePage.prototype.makeMarker = function (position, icon, title) {
        return new google.maps.Marker({
            position: position,
            map: this.map,
            icon: icon,
            title: title,
            optimized: false
        });
    };
    NavigatePage.prototype.calculateDistance = function (lat1, lat2, long1, long2) {
        var p = 0.017453292519943295; // Math.PI / 180
        var c = Math.cos;
        var a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((long1 - long2) * p))) / 2;
        var dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
        return dis * 1000;
    };
    NavigatePage.prototype.loadMarkers = function () {
        var markerOptions;
        var marker;
        console.log(this.locations);
        for (var i = 0; i < this.locations.length; i++) {
            markerOptions = {
                map: this.map,
                position: {
                    lat: this.locations[i].lat,
                    lng: this.locations[i].lon,
                },
                icon: {
                    path: 'M18 2c1.1 0 2 0.9 2 2l0 13c0 1.1-0.9 2-2 2l-4 0 -3 3 -3-3 -4 0c-1.1 0-2-0.9-2-2l0-13c0-1.1 0.9-2 2-2l14 0Zm-7 10.9l2.8 1.7 -0.7-3.2 2.5-2.1 -3.2-0.3 -1.3-3 -1.3 3 -3.2 0.3 2.5 2.1 -0.7 3.2 2.8-1.7Z',
                    scale: 1.6363636363636363636363636364,
                    anchor: new google.maps.Point(11, 22),
                    fillOpacity: 1,
                    fillColor: '#be1620',
                    strokeOpacity: 0
                }
            };
            marker = new google.maps.Marker(markerOptions);
        }
    };
    NavigatePage.prototype.footerExpanded = function () {
        console.log('Footer expanded!');
    };
    NavigatePage.prototype.footerCollapsed = function () {
        console.log('Footer collapsed!');
    };
    NavigatePage.prototype.toggleFooter = function () {
        this.footerState = this.footerState == __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed ? __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Expanded : __WEBPACK_IMPORTED_MODULE_5_ionic_pullup__["a" /* IonPullUpFooterState */].Collapsed;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChild */])('map'),
        __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]) === "function" && _a || Object)
    ], NavigatePage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChild */])('directionsPanel'),
        __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]) === "function" && _b || Object)
    ], NavigatePage.prototype, "directionsPanel", void 0);
    NavigatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-navigate',template:/*ion-inline-start:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/navigate/navigate.html"*/'<!--\n  Generated template for the NavigatePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar hideBackButton="true">\n        <ion-title>Navigate</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-card>\n        <ion-card-content padding-top>\n            <button ion-button full small class="collapsible">\n        <ion-icon name="arrow-round-up"></ion-icon>\n      </button>\n            <div #directionsPanel></div>\n        </ion-card-content>\n    </ion-card>\n    <div #map id="map"></div>\n\n    <button color="dark" ion-button block (click)="confirmEndTour()">\n    End Tour\n  </button>\n\n    <!-- <ion-pullup (onExpand)="footerExpanded()" (onCollapse)="footerCollapsed()" [(state)]="footerState">\n        <ion-pullup-tab [footer]="pullup" (tap)="toggleFooter()">\n            <ion-icon name="arrow-up" *ngIf="footerState == 0"></ion-icon>\n            <ion-icon name="arrow-down" *ngIf="footerState == 1"></ion-icon>\n        </ion-pullup-tab>\n        <ion-toolbar color="primary" (tap)="toggleFooter()">\n            <ion-title>Footer</ion-title>\n        </ion-toolbar>\n        <ion-content>\n            ... FOOTER CONTENT ...\n        </ion-content>\n    </ion-pullup> -->\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Drive-Archive/Documents/West Belfast Tours/src/pages/navigate/navigate.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_6__providers_locations_locations__["a" /* LocationsProvider */]]
        }),
        __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_6__providers_locations_locations__["a" /* LocationsProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__providers_locations_locations__["a" /* LocationsProvider */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */]) === "function" && _k || Object])
    ], NavigatePage);
    return NavigatePage;
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
}());

//# sourceMappingURL=navigate.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(364);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pipes_distance_distance__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_phonegap_local_notification_ngx__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tab2_tab2__ = __webpack_require__(347);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ionic_pullup__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_local_notifications_ngx__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_locations_locations__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_tabs_tabs__ = __webpack_require__(357);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tab2_tab2__["a" /* Tab2Page */],
                __WEBPACK_IMPORTED_MODULE_17__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pipes_distance_distance__["a" /* DistancePipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_14_ionic_pullup__["b" /* IonPullupModule */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/location-details/location-details.module#LocationDetailsPageModule', name: 'LocationDetailsPage', segment: 'location-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tab1/tab1.module#Tab1PageModule', name: 'Tab1Page', segment: 'tab1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/thankyou/thankyou.module#ThankyouPageModule', name: 'ThankyouPage', segment: 'thankyou', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/navigate/navigate.module#NavigatePageModule', name: 'NavigatePage', segment: 'navigate', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tab2_tab2__["a" /* Tab2Page */],
                __WEBPACK_IMPORTED_MODULE_17__pages_tabs_tabs__["a" /* TabsPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_local_notifications_ngx__["a" /* LocalNotifications */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_phonegap_local_notification_ngx__["a" /* PhonegapLocalNotification */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_push__["a" /* Push */],
                __WEBPACK_IMPORTED_MODULE_16__providers_locations_locations__["a" /* LocationsProvider */]
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */],
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NO_ERRORS_SCHEMA */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DistancePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the DistancePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var DistancePipe = /** @class */ (function () {
    function DistancePipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    DistancePipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (value) {
            return value + ' m';
        }
        else {
            return null;
        }
    };
    DistancePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* Pipe */])({
            name: 'distancePipe',
        })
    ], DistancePipe);
    return DistancePipe;
}());

//# sourceMappingURL=distance.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the LocationsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LocationsProvider = /** @class */ (function () {
    function LocationsProvider(http) {
        this.http = http;
        this.media = [];
        this.load();
        console.log('Hello LocationsProvider Provider');
    }
    LocationsProvider.prototype.load = function () {
        var _this = this;
        if (this.data) {
            // already loaded data
            console.log("Already loaded");
            return Promise.resolve(this.data);
        }
        // don't have the data yet
        return new Promise(function (resolve) {
            _this.data = [];
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            _this.http.get('http://192.168.0.153:7071/api/Locations?app=dc&state=foreground')
                .subscribe(function (data) {
                _this.first_index = data[0].id;
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                for (var i = 0; i < data.length; i++) {
                    //console.log(data[i]);//data;
                    _this.data[i] = {
                        id: data[i].id,
                        description: data[i].description,
                        name: data[i].name,
                        lat: data[i].lat,
                        lon: data[i].lon,
                        img: null,
                        notification: null,
                        distance: null
                    };
                    _this.loadData(i + _this.first_index);
                }
                //console.log(this.data);
                resolve(_this.data);
            });
        });
    };
    LocationsProvider.prototype.loadDistances = function (lat, lon) {
        // if (this.distances) {
        //   // already loaded data
        //   console.log("Already loaded");
        //   return Promise.resolve(this.distances);
        // }
        var _this = this;
        // don't have the data yet
        return new Promise(function (resolve) {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            _this.http.get('http://192.168.0.153:7071/api/Distances?position=' + lat + ',' + lon + '&app=dc&state=foreground')
                .subscribe(function (distances) {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                for (var i = 0; i < distances.length; i++) {
                    //subscribe distances
                    _this.data[i].distance = distances[i].distance;
                }
            });
            resolve(_this.data);
        });
    };
    LocationsProvider.prototype.updateDistances = function (lat, lon, locations) {
        var _this = this;
        // don't have the data yet
        return new Promise(function (resolve) {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            _this.http.get('http://192.168.0.153:7071/api/Distances?position=' + lat + ',' + lon + '&app=dc&state=foreground')
                .subscribe(function (distances) {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                for (var i = 0; i < locations.length; i++) {
                    //subscribe distances
                    var id = locations[i].id;
                    if (id > -1)
                        locations[i].distance = distances[_this.isItemInArray(distances, id)].distance;
                    //console.log(locations[i].distance);
                    //console.log();
                    //this.locations[i].distance = this.distances[this.isItemInArray(this.distances,id)].distance;
                }
            });
            resolve(locations);
        });
    };
    LocationsProvider.prototype.isItemInArray = function (array, item) {
        for (var i = 0; i < array.length; i++) {
            // depends on the format of the array
            if (array[i].id == item) {
                return i; // Found it
            }
        }
        return -1; // Not found
    };
    //load data of all locations
    LocationsProvider.prototype.loadData = function (id) {
        var _this = this;
        if (this.distances) {
            //   // already loaded data
            //   console.log("Already loaded");
            return Promise.resolve(this.distances);
        }
        // don't have the data yet
        return new Promise(function (resolve) {
            // We're using Angular HTTP provider to request the data,
            // then on the response, it'll map the JSON data to a parsed JS object.
            // Next, we process the data and resolve the promise with the new data.
            _this.http.get('http://192.168.0.153:7071/api/Data?app=dc&location=' + id)
                .subscribe(function (data) {
                // we've got back the raw data, now generate the core schedule data
                // and save the data for later reference
                var index = id - _this.first_index;
                for (var i = 0; i < data.length; i++) {
                    //subscribe image
                    if (data[i].data_type == "data:image/jpeg;"
                        || data[i].data_type == "data:image/png;"
                        || data[i].data_type == "data:image/jpg;") {
                        _this.data[index].img = data[i].data_type + data[i].data;
                    }
                }
                resolve(_this.data);
            });
        });
    };
    LocationsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
    ], LocationsProvider);
    return LocationsProvider;
    var _a;
}());

//# sourceMappingURL=locations.js.map

/***/ })

},[359]);
//# sourceMappingURL=main.js.map